from flask import Flask, request, render_template
import pymysql

db = pymysql.connect("mysql_db_fina", "root", "root", "lol_characters", 3306)

app2 = Flask(__name__)

@app2.route('/')
def someName():
    cursor = db.cursor()
    sql = "SELECT * FROM characters"
    cursor.execute(sql)
    results = cursor.fetchall()
    return render_template('index.html', results=results)

if __name__ == '__main__':
    app2.run(debug=True, host='0.0.0.0')
